﻿#include <iostream>
#include <math.h>

class Vector {
public:
	Vector() : x(6.8), y(5.2), z(9.4) {
	}


	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z) {
	}


	double Show() {

		double Length = sqrt(x * x + y * y + z * z);


		return Length;
	}


private:

	double x = 0;
	double y = 0;
	double z = 0;
};

int main()
{
	Vector v;
	std::cout << v.Show();
}
